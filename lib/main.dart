import 'package:flutter/material.dart';
import 'package:flutter_video_full_controller/video_player.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return VideoApp();
  }
}